package me.jvt.http.mediatype;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Representation of a Media Type for use with content negotiation.
 *
 * @see <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec3.html#sec3.7">HTTP/1.1 section
 *     3.7</a>
 */
public class MediaType {

  /** Public constant media type that includes all media ranges (i.e. "&#42;/&#42;"). */
  public static final MediaType WILDCARD;

  /** A String equivalent of {@link MediaType#WILDCARD}. */
  public static final String ALL_VALUE = "*/*";

  /** Public constant media type for {@code application/atom+xml}. */
  public static final MediaType APPLICATION_ATOM_XML;

  /** A String equivalent of {@link MediaType#APPLICATION_ATOM_XML}. */
  public static final String APPLICATION_ATOM_XML_VALUE = "application/atom+xml";

  /**
   * Public constant media type for {@code application/cbor}.
   *
   * @since 5.2
   */
  public static final MediaType APPLICATION_CBOR;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_CBOR}.
   *
   * @since 5.2
   */
  public static final String APPLICATION_CBOR_VALUE = "application/cbor";

  /** Public constant media type for {@code application/x-www-form-urlencoded}. */
  public static final MediaType APPLICATION_FORM_URLENCODED;

  /** A String equivalent of {@link MediaType#APPLICATION_FORM_URLENCODED}. */
  public static final String APPLICATION_FORM_URLENCODED_VALUE =
      "application/x-www-form-urlencoded";

  /** Public constant media type for {@code application/json}. */
  public static final MediaType APPLICATION_JSON;

  /** A String equivalent of {@link MediaType#APPLICATION_JSON}. */
  public static final String APPLICATION_JSON_VALUE = "application/json";

  /** Public constant media type for {@code application/octet-stream}. */
  public static final MediaType APPLICATION_OCTET_STREAM;

  /** A String equivalent of {@link MediaType#APPLICATION_OCTET_STREAM}. */
  public static final String APPLICATION_OCTET_STREAM_VALUE = "application/octet-stream";

  /**
   * Public constant media type for {@code application/pdf}.
   *
   * @since 4.3
   */
  public static final MediaType APPLICATION_PDF;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_PDF}.
   *
   * @since 4.3
   */
  public static final String APPLICATION_PDF_VALUE = "application/pdf";

  /**
   * Public constant media type for {@code application/problem+json}.
   *
   * @since 5.0
   * @see <a href="https://tools.ietf.org/html/rfc7807#section-6.1">Problem Details for HTTP APIs,
   *     6.1. application/problem+json</a>
   */
  public static final MediaType APPLICATION_PROBLEM_JSON;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_PROBLEM_JSON}.
   *
   * @since 5.0
   */
  public static final String APPLICATION_PROBLEM_JSON_VALUE = "application/problem+json";
  /**
   * Public constant media type for {@code application/problem+xml}.
   *
   * @since 5.0
   * @see <a href="https://tools.ietf.org/html/rfc7807#section-6.2">Problem Details for HTTP APIs,
   *     6.2. application/problem+xml</a>
   */
  public static final MediaType APPLICATION_PROBLEM_XML;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_PROBLEM_XML}.
   *
   * @since 5.0
   */
  public static final String APPLICATION_PROBLEM_XML_VALUE = "application/problem+xml";

  /**
   * Public constant media type for {@code application/rss+xml}.
   *
   * @since 4.3.6
   */
  public static final MediaType APPLICATION_RSS_XML;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_RSS_XML}.
   *
   * @since 4.3.6
   */
  public static final String APPLICATION_RSS_XML_VALUE = "application/rss+xml";

  /**
   * Public constant media type for {@code application/x-ndjson}.
   *
   * @since 5.3
   */
  public static final MediaType APPLICATION_NDJSON;

  /**
   * A String equivalent of {@link MediaType#APPLICATION_NDJSON}.
   *
   * @since 5.3
   */
  public static final String APPLICATION_NDJSON_VALUE = "application/x-ndjson";

  /** Public constant media type for {@code application/xhtml+xml}. */
  public static final MediaType APPLICATION_XHTML_XML;

  /** A String equivalent of {@link MediaType#APPLICATION_XHTML_XML}. */
  public static final String APPLICATION_XHTML_XML_VALUE = "application/xhtml+xml";

  /** Public constant media type for {@code application/xml}. */
  public static final MediaType APPLICATION_XML;

  /** A String equivalent of {@link MediaType#APPLICATION_XML}. */
  public static final String APPLICATION_XML_VALUE = "application/xml";

  /** Public constant media type for {@code image/gif}. */
  public static final MediaType IMAGE_GIF;

  /** A String equivalent of {@link MediaType#IMAGE_GIF}. */
  public static final String IMAGE_GIF_VALUE = "image/gif";

  /** Public constant media type for {@code image/jpeg}. */
  public static final MediaType IMAGE_JPEG;

  /** A String equivalent of {@link MediaType#IMAGE_JPEG}. */
  public static final String IMAGE_JPEG_VALUE = "image/jpeg";

  /** Public constant media type for {@code image/png}. */
  public static final MediaType IMAGE_PNG;

  /** A String equivalent of {@link MediaType#IMAGE_PNG}. */
  public static final String IMAGE_PNG_VALUE = "image/png";

  /** Public constant media type for {@code multipart/form-data}. */
  public static final MediaType MULTIPART_FORM_DATA;

  /** A String equivalent of {@link MediaType#MULTIPART_FORM_DATA}. */
  public static final String MULTIPART_FORM_DATA_VALUE = "multipart/form-data";

  /**
   * Public constant media type for {@code multipart/mixed}.
   *
   * @since 5.2
   */
  public static final MediaType MULTIPART_MIXED;

  /**
   * A String equivalent of {@link MediaType#MULTIPART_MIXED}.
   *
   * @since 5.2
   */
  public static final String MULTIPART_MIXED_VALUE = "multipart/mixed";

  /**
   * Public constant media type for {@code multipart/related}.
   *
   * @since 5.2.5
   */
  public static final MediaType MULTIPART_RELATED;

  /**
   * A String equivalent of {@link MediaType#MULTIPART_RELATED}.
   *
   * @since 5.2.5
   */
  public static final String MULTIPART_RELATED_VALUE = "multipart/related";

  /**
   * Public constant media type for {@code text/event-stream}.
   *
   * @since 4.3.6
   * @see <a href="https://www.w3.org/TR/eventsource/">Server-Sent Events W3C recommendation</a>
   */
  public static final MediaType TEXT_EVENT_STREAM;

  /**
   * A String equivalent of {@link MediaType#TEXT_EVENT_STREAM}.
   *
   * @since 4.3.6
   */
  public static final String TEXT_EVENT_STREAM_VALUE = "text/event-stream";

  /** Public constant media type for {@code text/html}. */
  public static final MediaType TEXT_HTML;

  /** A String equivalent of {@link MediaType#TEXT_HTML}. */
  public static final String TEXT_HTML_VALUE = "text/html";

  /**
   * Public constant media type for {@code text/markdown}.
   *
   * @since 4.3
   */
  public static final MediaType TEXT_MARKDOWN;

  /**
   * A String equivalent of {@link MediaType#TEXT_MARKDOWN}.
   *
   * @since 4.3
   */
  public static final String TEXT_MARKDOWN_VALUE = "text/markdown";

  /** Public constant media type for {@code text/plain}. */
  public static final MediaType TEXT_PLAIN;

  /** A String equivalent of {@link MediaType#TEXT_PLAIN}. */
  public static final String TEXT_PLAIN_VALUE = "text/plain";

  /** Public constant media type for {@code text/xml}. */
  public static final MediaType TEXT_XML;

  /** A String equivalent of {@link MediaType#TEXT_XML}. */
  public static final String TEXT_XML_VALUE = "text/xml";
  /**
   * A {@link Comparator} for providing sorting of {@link MediaType}s based on specificity and
   * quality values.
   *
   * @see MediaTypeComparator
   */
  public static final Comparator<MediaType> COMPARATOR = new MediaTypeComparator();

  // https://tools.ietf.org/html/rfc7230#section-3.2.6
  private static final String TCHAR_SET = "[!#\\$%&'*/ +-.^_`|~0-9a-zA-Z]";
  private static final String QVALUE_PATTERN_S = "[01](?:|\\.[0-9]{1,3})";
  private static final Pattern QVALUE_PATTERN =
      Pattern.compile(String.format("(%s)", QVALUE_PATTERN_S));
  private static final String WEIGHT_PATTERN = String.format("q=%s", QVALUE_PATTERN_S);
  private static final String QUOTED_STRING_PATTERN = "\"[^\"]+\"";
  private static final String PARAMETER_PATTERN =
      String.format("%s+=(?:%s+|%s+)", TCHAR_SET, TCHAR_SET, QUOTED_STRING_PATTERN);
  private static final Pattern PARAMETERS_PATTERN =
      Pattern.compile(String.format("(\\s*;\\s*(%s|%s))", PARAMETER_PATTERN, WEIGHT_PATTERN));
  private static final Pattern TYPE_AND_SUBTYPE_PATTERN =
      Pattern.compile(String.format("(%s+)/(%s+)(.*)", TCHAR_SET, TCHAR_SET));

  private static final String CANNOT_PARSE = "Could not parse a valid MediaType";

  static final List<MediaType> DEFAULT_PARSED_MEDIA_TYPES;

  private static final String WILDCARD_SEGMENT = "*";
  private static final double DEFAULT_QUALITY = 1.0;
  private static final String QUALITY_PARAMETER = "q";

  static {
    // Not using "valueOf' to avoid static init cost
    WILDCARD = new MediaType(WILDCARD_SEGMENT, WILDCARD_SEGMENT);
    APPLICATION_ATOM_XML = new MediaType("application", "atom+xml");
    APPLICATION_CBOR = new MediaType("application", "cbor");
    APPLICATION_FORM_URLENCODED = new MediaType("application", "x-www-form-urlencoded");
    APPLICATION_JSON = new MediaType("application", "json");
    APPLICATION_NDJSON = new MediaType("application", "x-ndjson");
    APPLICATION_OCTET_STREAM = new MediaType("application", "octet-stream");
    APPLICATION_PDF = new MediaType("application", "pdf");
    APPLICATION_PROBLEM_JSON = new MediaType("application", "problem+json");
    APPLICATION_PROBLEM_XML = new MediaType("application", "problem+xml");
    APPLICATION_RSS_XML = new MediaType("application", "rss+xml");
    APPLICATION_XHTML_XML = new MediaType("application", "xhtml+xml");
    APPLICATION_XML = new MediaType("application", "xml");
    IMAGE_GIF = new MediaType("image", "gif");
    IMAGE_JPEG = new MediaType("image", "jpeg");
    IMAGE_PNG = new MediaType("image", "png");
    MULTIPART_FORM_DATA = new MediaType("multipart", "form-data");
    MULTIPART_MIXED = new MediaType("multipart", "mixed");
    MULTIPART_RELATED = new MediaType("multipart", "related");
    TEXT_EVENT_STREAM = new MediaType("text", "event-stream");
    TEXT_HTML = new MediaType("text", "html");
    TEXT_MARKDOWN = new MediaType("text", "markdown");
    TEXT_PLAIN = new MediaType("text", "plain");
    TEXT_XML = new MediaType("text", "xml");
    DEFAULT_PARSED_MEDIA_TYPES = Collections.singletonList(MediaType.WILDCARD);
  }

  private final String type;
  private final String subtype;

  private final Map<String, String> parameters;
  private final String subtypeSuffix;

  /**
   * Parse a given string representation of a media type as a {@link MediaType}
   *
   * @param mediaType the string representation of the media type
   * @return the parsed {@link MediaType}
   * @throws IllegalArgumentException if the media type could not be parsed
   */
  public static MediaType valueOf(String mediaType) {
    if (null == mediaType || mediaType.isEmpty()) {
      throw new IllegalArgumentException(CANNOT_PARSE);
    }

    if (mediaType.contains(",")) {
      throw new IllegalArgumentException("Multiple MediaTypes resolved");
    }

    Matcher m = TYPE_AND_SUBTYPE_PATTERN.matcher(mediaType);
    if (!m.matches()) {
      throw new IllegalArgumentException(CANNOT_PARSE);
    }

    Map<String, String> parameters = new HashMap<>();
    Matcher paramMatcher = PARAMETERS_PATTERN.matcher(m.group(3));

    while (paramMatcher.find()) {
      String group = paramMatcher.group();
      String[] split = group.split("=");
      String prefix = split[0].replaceAll("^[\\s;]*", "");

      if (QUALITY_PARAMETER.equalsIgnoreCase(prefix)) {
        Matcher qvalueMatcher = QVALUE_PATTERN.matcher(split[1]);
        if (qvalueMatcher.matches()) {
          parameters.put(QUALITY_PARAMETER, qvalueMatcher.group());
        }
      } else {
        parameters.put(prefix, split[1]);
      }
    }
    return new MediaType(m.group(1), m.group(2), parameters);
  }

  /**
   * Construct a new {@link MediaType}.
   *
   * @param type the parent type of the media type
   * @param subtype the subtype of the media type
   */
  public MediaType(String type, String subtype) {
    this(type, subtype, Collections.emptyMap());
  }

  /**
   * Construct a new {@link MediaType}.
   *
   * @param type the parent type of the media type
   * @param subtype the subtype of the media type
   * @param parameters any parameters that are set on the media type
   */
  public MediaType(String type, String subtype, Map<String, String> parameters) {
    if (null == type || type.isEmpty()) {
      throw new IllegalArgumentException("Type cannot be null or empty");
    }
    if (null == subtype || subtype.isEmpty()) {
      throw new IllegalArgumentException("Subtype cannot be null or empty");
    }
    this.type = type;
    this.subtype = subtype;
    String[] subtypeSuffixParts = subtype.split("\\+");
    if (subtypeSuffixParts.length > 1) {
      this.subtypeSuffix = subtypeSuffixParts[1];
    } else {
      this.subtypeSuffix = null;
    }
    if (isWildcardType() && !isWildcardSubtype()) {
      throw new IllegalArgumentException("Type cannot be wildcard");
    }
    this.parameters = Collections.unmodifiableMap(parameters);
  }

  /**
   * Parse a given <code>Accept</code> header as the requested {@link MediaType}s.
   *
   * @param acceptHeader the String representation of the <code>Accept</code> header
   * @return the parsed {@link MediaType}s. Instead of empty, will always return a singleton with a
   *     <code>MediaType.MEDIA_TYPE_WILDCARD</code>.
   */
  public static List<MediaType> parseAcceptHeader(String acceptHeader) {
    List<MediaType> mediaTypes = new ArrayList<>();
    if (null == acceptHeader) {
      return mediaTypes;
    }

    for (String part : acceptHeader.split(",")) {
      try {
        MediaType mediaType = MediaType.valueOf(part.replaceFirst(" ", ""));
        mediaTypes.add(mediaType);
      } catch (IllegalArgumentException e) {
        // ignore
      }
    }

    return mediaTypes;
  }

  /**
   * Convert a Spring Web {@link org.springframework.http.MediaType} to a {@link MediaType}.
   *
   * @param mediaType the spring-web {@link org.springframework.http.MediaType}
   * @return the parsed {@link MediaType}
   */
  public static MediaType from(org.springframework.http.MediaType mediaType) {
    return MediaType.valueOf(mediaType.toString());
  }

  /**
   * Get the parent type of the media type.
   *
   * @return the parent type of the media type.
   */
  public String getType() {
    return type;
  }

  /**
   * Get the subtype suffix of the media type.
   *
   * @return the subtype suffix of the media type, or <code>null</code>
   */
  public String getSubtypeSuffix() {
    return subtypeSuffix;
  }

  /**
   * Get the subtype of the media type.
   *
   * @return the subtype of the media type.
   */
  public String getSubtype() {
    return subtype;
  }

  /**
   * Get the parameters associated with the media type.
   *
   * @return the parameters
   */
  public Map<String, String> getParameters() {
    return parameters;
  }

  /**
   * Return the quality factor, as indicated by a {@code q} parameter, if any. Defaults to {@code
   * 1.0}.
   *
   * @return the quality factor as double value
   */
  public double getQualityValue() {
    String q = parameters.get(QUALITY_PARAMETER);
    if (null == q) {
      return DEFAULT_QUALITY;
    }
    try {
      DecimalFormat df = new DecimalFormat("#.###");
      String formatted = df.format(Double.parseDouble(q));
      return Double.parseDouble(formatted);
    } catch (NumberFormatException e) {
      return DEFAULT_QUALITY;
    }
  }

  /**
   * Checks if the primary type is a wildcard.
   *
   * @return if the type is a wildcard
   */
  public boolean isWildcardType() {
    return WILDCARD_SEGMENT.equals(type);
  }

  /**
   * Checks if the subtype is a wildcard.
   *
   * @return if the subtype is a wildcard
   */
  public boolean isWildcardSubtype() {
    return WILDCARD_SEGMENT.equals(subtype) || subtype.startsWith(WILDCARD_SEGMENT + "+");
  }

  /**
   * Checks whether this {@link MediaType} is compatible.
   *
   * <p>For instance <code>application/*+json</code> is compatible with <code>application/json
   * </code> but not <code>application/pdf</code>.
   *
   * @param other the {@link MediaType} to compare with
   * @return whether they are compatible or not
   */
  public boolean isCompatible(MediaType other) {
    if (other == null) {
      return false;
    }

    if (isWildcardType() || other.isWildcardType()) {
      return true;
    }
    if (type.equalsIgnoreCase(other.getType())) {
      if (subtype.equalsIgnoreCase(other.getSubtype())) {
        return true;
      }

      if (isWildcardSubtype() || other.isWildcardSubtype()) {
        String thisSuffix = getSubtypeSuffix();
        String otherSuffix = other.getSubtypeSuffix();

        if (getSubtype().equals(WILDCARD_SEGMENT) || other.getSubtype().equals(WILDCARD_SEGMENT)) {
          return true;
        } else if (isWildcardSubtype() && thisSuffix != null) {
          return thisSuffix.equalsIgnoreCase(other.getSubtype())
              || thisSuffix.equalsIgnoreCase(otherSuffix);
        } else if (other.isWildcardSubtype() && otherSuffix != null) {
          return getSubtype().equalsIgnoreCase(otherSuffix)
              || otherSuffix.equalsIgnoreCase(thisSuffix);
        }
      }
    }

    return false;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MediaType mediaType = (MediaType) o;
    return Objects.equals(type, mediaType.type)
        && Objects.equals(subtype, mediaType.subtype)
        && Objects.equals(parameters, mediaType.parameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, subtype, parameters);
  }

  @Override
  public String toString() {
    String toString = type + "/" + subtype;

    for (Map.Entry<String, String> param : parameters.entrySet()) {
      toString = String.format("%s;%s=%s", toString, param.getKey(), param.getValue());
    }
    return toString;
  }
}
