package me.jvt.http.mediatype;

import java.util.ArrayList;
import java.util.List;

/** A utility to parse {@link MediaType}s from a given <code>Accept</code> header format. */
public class AcceptHeaderParser {

  /**
   * Parse a given <code>Accept</code> header as the requested {@link MediaType}s.
   *
   * @param acceptHeader the String representation of the <code>Accept</code> header
   * @return the parsed {@link MediaType}s. Instead of empty, will always return a singleton with a
   *     <code>MediaType.MEDIA_TYPE_WILDCARD</code>.
   */
  public List<MediaType> parse(String acceptHeader) {
    return MediaType.parseAcceptHeader(acceptHeader);
  }

  /**
   * Parse multiple <code>Accept</code> header values as the requested {@link MediaType}s.
   *
   * @param acceptHeaders multiple String representations of the <code>Accept</code> header
   * @return the parsed {@link MediaType}s. Instead of empty, will always return a singleton with a
   *     <code>MediaType.MEDIA_TYPE_WILDCARD</code>.
   */
  public List<MediaType> parse(List<String> acceptHeaders) {
    List<MediaType> mediaTypes = new ArrayList<>();

    if (null == acceptHeaders || acceptHeaders.isEmpty()) {
      return mediaTypes;
    }

    for (String accept : acceptHeaders) {
      mediaTypes.addAll(MediaType.parseAcceptHeader(accept));
    }

    return mediaTypes;
  }
}
