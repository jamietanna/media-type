package me.jvt.http.mediatype;

import java.util.Comparator;

/**
 * A {@link Comparator} to allow sorting {@link MediaType}s by how specific the requested {@link
 * MediaType} is, and the quality value used to control the {@link MediaType} precedence.
 *
 * <p>Adapted from <a
 * href="https://github.com/spring-projects/spring-framework/blob/v5.3.2/spring-core/src/main/java/org/springframework/util/MimeType.java#L625-L664">spring-web's
 * MimeType.SpecificityComparator</a> and <a
 * href="https://github.com/spring-projects/spring-framework/blob/dfb7ca733ad309b35040e0027fb7a2f10f3a196a/spring-web/src/main/java/org/springframework/http/MediaType.java#L823-L838">spring-web's
 * MediaType.SPECIFICITY_COMPARATOR</a>.
 */
public class MediaTypeComparator implements Comparator<MediaType> {

  public int compare(MediaType lhs, MediaType rhs) {
    if (lhs.isWildcardType() && !rhs.isWildcardType()) { // */* < audio/*
      return 1;
    } else if (rhs.isWildcardType() && !lhs.isWildcardType()) { // audio/* > */*
      return -1;
    } else if (!lhs.getType().equals(rhs.getType())) { // audio/basic == text/html
      return 0;
    } else { // lhs.getType().equals(rhs.getType())
      if (lhs.isWildcardSubtype() && !rhs.isWildcardSubtype()) { // audio/* < audio/basic
        return 1;
      } else if (rhs.isWildcardSubtype() && !lhs.isWildcardSubtype()) { // audio/basic > audio/*
        return -1;
      } else { // audio/basic ~= audio/wave || rhs.getSubtype().equals(mediaType2.getSubtype())
        double lhsQuality = lhs.getQualityValue();
        double rhsQuality = rhs.getQualityValue();
        int qualityComparison = Double.compare(rhsQuality, lhsQuality);
        if (qualityComparison != 0) {
          return qualityComparison; // audio/*;q=0.7 < audio/*;q=0.3
        }

        int paramsSize1 = lhs.getParameters().size();
        int paramsSize2 = rhs.getParameters().size();
        return Integer.compare(paramsSize2, paramsSize1); // audio/basic;level=1 < audio/basic
      }
    }
  }
}
