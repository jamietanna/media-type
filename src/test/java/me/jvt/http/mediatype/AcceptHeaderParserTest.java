package me.jvt.http.mediatype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class AcceptHeaderParserTest {

  private final AcceptHeaderParser parser = new AcceptHeaderParser();

  @Nested
  class SingleHeader {

    @Test
    void handlesSingleValue() {
      List<MediaType> actual = parser.parse("text/plain");

      assertThat(actual).containsExactly(MediaType.TEXT_PLAIN);
    }

    @Test
    void handlesMultipleValues() {
      List<MediaType> actual =
          parser.parse("text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c");

      assertThat(actual)
          .containsExactlyInAnyOrder(
              MediaType.valueOf("text/plain; q=0.5"),
              MediaType.TEXT_HTML,
              MediaType.valueOf("text/x-dvi; q=0.8"),
              MediaType.valueOf("text/x-c"));
    }

    @ParameterizedTest
    @NullAndEmptySource
    void returnsEmptyIfNullOrEmpty(String accept) {
      List<MediaType> actual = parser.parse(accept);

      assertThat(actual).isEmpty();
    }
  }

  @Nested
  class MultipleHeaders {

    @Test
    void handlesSingleValue() {
      List<MediaType> actual = parser.parse(Collections.singletonList("text/plain"));

      assertThat(actual).containsExactly(MediaType.TEXT_PLAIN);
    }

    @Test
    void handlesMultipleValues() {
      List<MediaType> actual = parser.parse(Arrays.asList("text/plain; q=0.5", "text/html"));

      assertThat(actual)
          .containsExactlyInAnyOrder(MediaType.valueOf("text/plain; q=0.5"), MediaType.TEXT_HTML);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void returnsEmptyIfNullOrEmpty(List<String> headers) {
      List<MediaType> actual = parser.parse(headers);

      assertThat(actual).isEmpty();
    }

    @Test
    void returnsParsedMediaTypeIfPresent() {
      List<MediaType> actual = parser.parse(Arrays.asList("", "text/plain"));

      assertThat(actual).containsExactlyInAnyOrder(MediaType.TEXT_PLAIN);
    }

    @Test
    void returnsWildcardIfPresent() {
      List<MediaType> actual = parser.parse(Collections.singletonList("*/*"));

      assertThat(actual).containsExactlyInAnyOrder(MediaType.WILDCARD);
    }

    @Test
    void returnsEmptyIfAllHeadersAreEmpty() {
      List<MediaType> actual = parser.parse(Arrays.asList("", ""));

      assertThat(actual).isEmpty();
    }
  }
}
