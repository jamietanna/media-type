package me.jvt.http.mediatype;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class MediaTypeTest {

  @Nested
  class Constructors {

    @Test
    void canBeConstructedWithTypeAndSubtype() {
      assertThatCode(() -> new MediaType("application", "json")).doesNotThrowAnyException();
    }

    @Test
    void canBeConstructedWithParameters() {
      assertThatCode(() -> new MediaType("application", "json", new HashMap<>()))
          .doesNotThrowAnyException();
    }

    @ParameterizedTest
    @NullAndEmptySource
    void throwsIllegalArgumentExceptionWhenNullOrEmptyType(String type) {
      assertThatThrownBy(() -> new MediaType(type, "json"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Type cannot be null or empty");
    }

    @ParameterizedTest
    @NullAndEmptySource
    void throwsIllegalArgumentExceptionWhenNullOrEmptySubtype(String subtype) {
      assertThatThrownBy(() -> new MediaType("application", subtype))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Subtype cannot be null or empty");
    }

    @Test
    void throwsIllegalArgumentExceptionWhenWildcardTypeButNotSubtype() {
      assertThatThrownBy(() -> new MediaType("*", "foo"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Type cannot be wildcard");
    }
  }

  @Nested
  class GetParameters {

    @Test
    void whenNotSetAreEmpty() {
      MediaType mediaType = new MediaType("b", "b");

      Map<String, String> actual = mediaType.getParameters();

      assertThat(actual).isEmpty();
    }

    @Test
    void whenSetAreReturned() {
      Map<String, String> parameters = Collections.singletonMap("key", "value");
      MediaType mediaType = new MediaType("b", "b", parameters);

      Map<String, String> actual = mediaType.getParameters();

      assertThat(actual).isEqualTo(parameters);
    }

    @Test
    void parametersAreCopied() {
      Map<String, String> parameters = Collections.singletonMap("key", "value");
      MediaType mediaType = new MediaType("b", "b", parameters);

      Map<String, String> actual = mediaType.getParameters();

      assertThat(actual).isNotSameAs(parameters);
    }
  }

  @Nested
  class GetSubtypeSuffix {
    @ParameterizedTest
    @ValueSource(strings = {"application/json", "text/plain", "*/*", "application/*"})
    void whenNoSuffixReturnsNull(String s) {
      MediaType mediaType = MediaType.valueOf(s);

      assertThat(mediaType.getSubtypeSuffix()).isNull();
    }

    @ParameterizedTest
    @ValueSource(strings = {"application/*+json", "application/vnd.api+json"})
    void whenSuffixReturnsSuffixValue(String s) {
      MediaType mediaType = MediaType.valueOf(s);

      assertThat(mediaType.getSubtypeSuffix()).isEqualTo("json");
    }
  }

  @Nested
  class GetQuality {

    @Test
    void whenNotSetDefaultsTo1() {
      MediaType mediaType = new MediaType("a", "b");

      assertThat(mediaType.getQualityValue()).isEqualTo(1.0);
    }

    @Test
    void whenNotLowercaseDefaultsTo1() {
      MediaType mediaType = new MediaType("a", "b", Collections.singletonMap("Q", "0.5"));

      assertThat(mediaType.getQualityValue()).isEqualTo(1.0);
    }

    @Test
    void whenSetInParameterMap() {
      MediaType mediaType = new MediaType("a", "b", Collections.singletonMap("q", "0.5"));

      assertThat(mediaType.getQualityValue()).isEqualTo(0.5);
    }

    @ParameterizedTest
    @NullAndEmptySource
    void whenNotParseableDefaultsTo1(String quality) {
      MediaType mediaType = new MediaType("a", "b", Collections.singletonMap("q", quality));

      assertThat(mediaType.getQualityValue()).isEqualTo(1.0);
    }

    @Test
    void trimsToThreeDecimalPlaces() {
      MediaType mediaType = new MediaType("a", "b", Collections.singletonMap("q", "0.1234"));

      assertThat(mediaType.getQualityValue()).isEqualTo(0.123);
    }
  }

  @Nested
  class IsCompatible {

    private MediaType lhs;
    private MediaType rhs;

    @Test
    void nullOtherIsNotSupported() {
      lhs = MediaType.WILDCARD;
      rhs = null;

      assertNotSupported();
    }

    @Test
    void wildcardTypeIsEqualToItself() {
      lhs = MediaType.WILDCARD;
      rhs = MediaType.WILDCARD;

      assertSupported();
    }

    @Test
    void wildcardTypeSupportsConcreteTypeWhenRhs() {
      lhs = new MediaType("audio", "*");
      rhs = MediaType.WILDCARD;

      assertSupported();
    }

    @Test
    void wildcardTypeSupportsConcreteTypeWhenLhs() {
      lhs = MediaType.WILDCARD;
      rhs = new MediaType("audio", "*");

      assertSupported();
    }

    @Test
    void doesNotMatchWhenDifferentTypes() {
      lhs = MediaType.APPLICATION_JSON;
      rhs = MediaType.MULTIPART_FORM_DATA;

      assertNotSupported();
    }

    @Test
    void doesNotMatchWhenSameTypeButDifferentSubtypes() {
      lhs = new MediaType("application", "basic");
      rhs = new MediaType("audio", "basic");

      assertNotSupported();
    }

    @Test
    void wildcardSubtypeIsEqualToItself() {
      lhs = new MediaType("audio", "*");
      rhs = new MediaType("audio", "*");

      assertSupported();
    }

    @Test
    void wildcardSubtypeSupportsConcreteTypeWhenLhs() {
      lhs = new MediaType("audio", "basic");
      rhs = new MediaType("audio", "*");

      assertSupported();
    }

    @Test
    void wildcardSubtypeSupportsConcreteTypeWhenRhs() {
      lhs = new MediaType("audio", "*");
      rhs = new MediaType("audio", "basic");

      assertSupported();
    }

    @Test
    void parametersAreIgnored() {
      lhs = new MediaType("audio", "basic", Collections.singletonMap("foo", "bar"));
      rhs = new MediaType("audio", "basic");

      assertSupported();
    }

    @Test
    void typeIsCaseSensitive() {
      lhs = new MediaType("APPLICATION", "json");
      rhs = new MediaType("application", "json");

      assertSupported();
    }

    @Test
    void subTypeIsCaseSensitive() {
      lhs = new MediaType("application", "JSON");
      rhs = new MediaType("application", "json");

      assertSupported();
    }

    @Test
    void suffixOnLhsIsEqualToWildcardOnRhs() {
      lhs = new MediaType("application", "vnd.foo+json");
      rhs = new MediaType("*", "*");

      assertSupported();
    }

    @Test
    void suffixOnRhsIsEqualToWildcardOnLhs() {
      lhs = new MediaType("*", "*");
      rhs = new MediaType("application", "vnd.foo+json");

      assertSupported();
    }

    @Test
    void suffixOnLhsIsEqualToWildcardSubTypeOnRhs() {
      lhs = new MediaType("application", "vnd.foo+json");
      rhs = new MediaType("application", "*");

      assertSupported();
    }

    @Test
    void suffixOnRhsIsEqualToWildcardSubTypeOnLhs() {
      lhs = new MediaType("application", "*");
      rhs = new MediaType("application", "vnd.foo+json");

      assertSupported();
    }

    @Test
    void differentSubtypeSuffixAlwaysReturnsFalse() {
      lhs = new MediaType("application", "json+json");
      rhs = new MediaType("application", "xml+json");

      assertNotSupported();
    }

    @Test
    void suffixIsOnlySupportedWhenWithAWildcard() {
      lhs = new MediaType("application", "json");
      rhs = new MediaType("application", "vnd.foo+json");

      assertNotSupported();
    }

    @Test
    void suffixIsSupportedWhenWildcardSuffix() {
      lhs = new MediaType("application", "json");
      rhs = new MediaType("application", "*+json");

      assertSupported();
    }

    @Test
    void suffixIsNotSupportedWhenWildcardSuffixOnWrongSubTypeOnLhs() {
      lhs = new MediaType("application", "*+json");
      rhs = new MediaType("application", "xml");

      assertNotSupported();
    }

    @Test
    void suffixIsNotSupportedWhenWildcardSuffixOnWrongSubTypeOnRhs() {
      lhs = new MediaType("application", "xml");
      rhs = new MediaType("application", "*+json");

      assertNotSupported();
    }

    void assertSupported() {
      AssertionsForClassTypes.assertThat(lhs.isCompatible(rhs)).isTrue();
    }

    void assertNotSupported() {
      AssertionsForClassTypes.assertThat(lhs.isCompatible(rhs)).isFalse();
    }
  }

  @Nested
  class IsWildcardType {

    @Test
    void returnsFalseWhenNotWildcard() {
      MediaType mediaType = new MediaType("application", "foo");

      assertThat(mediaType.isWildcardType()).isFalse();
    }

    @Test
    void returnsTrueWhenWildcard() {
      MediaType mediaType = new MediaType("*", "*");

      assertThat(mediaType.isWildcardType()).isTrue();
    }
  }

  @Nested
  class IsWildcardSubtype {

    @Test
    void returnsFalseWhenNotWildcard() {
      MediaType mediaType = new MediaType("application", "foo");

      assertThat(mediaType.isWildcardSubtype()).isFalse();
    }

    @Test
    void returnsTrueWhenWildcard() {
      MediaType mediaType = new MediaType("application", "*");

      assertThat(mediaType.isWildcardSubtype()).isTrue();
    }

    @Test
    void returnsTrueWhenSuffix() {
      MediaType mediaType = new MediaType("application", "*+json");

      assertThat(mediaType.isWildcardSubtype()).isTrue();
    }
  }

  @Nested
  class Equality {

    @Test
    void areEqualWhenSameTypeSubtypeAndNoParams() {
      MediaType lhs = new MediaType("application", "foo");
      MediaType rhs = new MediaType("application", "foo");

      assertThat(lhs).isEqualTo(rhs);
    }

    @Test
    void areEqualWhenSameTypeSubtypeWithParams() {
      MediaType lhs = new MediaType("application", "foo", Collections.singletonMap("foo", "bar"));
      MediaType rhs = new MediaType("application", "foo", Collections.singletonMap("foo", "bar"));

      assertThat(lhs).isEqualTo(rhs);
    }

    @Test
    void areNotEqualWhenNotSameTypeOnLhs() {
      MediaType lhs = new MediaType("applications", "foo");
      MediaType rhs = new MediaType("application", "foo");

      assertThat(lhs).isNotEqualTo(rhs);
    }

    @Test
    void areNotEqualWhenNotSameSubtypeOnRhs() {
      MediaType lhs = new MediaType("application", "foo");
      MediaType rhs = new MediaType("applications", "foo");

      assertThat(lhs).isNotEqualTo(rhs);
    }

    @Test
    void areNotEqualWhenNotSameSubtypeOnLhs() {
      MediaType lhs = new MediaType("application", "fool");
      MediaType rhs = new MediaType("application", "foo");

      assertThat(lhs).isNotEqualTo(rhs);
    }

    @Test
    void areNotEqualWhenNotSameTypeOnRhs() {
      MediaType lhs = new MediaType("application", "foo");
      MediaType rhs = new MediaType("application", "fool");

      assertThat(lhs).isNotEqualTo(rhs);
    }

    @Test
    void areNotEqualWhenDifferentParameters() {
      MediaType lhs = new MediaType("application", "foo");
      MediaType rhs = new MediaType("application", "foo", Collections.singletonMap("foo", "bar"));

      assertThat(lhs).isNotEqualTo(rhs);
    }
  }

  @Nested
  class ValueOf {

    @ParameterizedTest
    @NullAndEmptySource
    void throwsIllegalArgumentExceptionWhenNullOrEmpty(String mediaType) {
      assertThatThrownBy(() -> MediaType.valueOf(mediaType))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Could not parse a valid MediaType");
    }

    @Test
    void throwsIllegalArgumentExceptionWhenNoSlash() {
      assertThatThrownBy(() -> MediaType.valueOf("application"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Could not parse a valid MediaType");
    }

    @Test
    void throwsIllegalArgumentExceptionWhenNoSubType() {
      assertThatThrownBy(() -> MediaType.valueOf("application/"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Could not parse a valid MediaType");
    }

    @Test
    void throwsIllegalArgumentExceptionWhenMultipleItems() {
      assertThatThrownBy(() -> MediaType.valueOf("audio/*; q=0.2, audio/basic"))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Multiple MediaTypes resolved");
    }

    @ParameterizedTest
    @ValueSource(
        strings = {
          "application/json",
          "multipart/form-data",
          "foo/bar",
          "application/vnd.me.jvt.api.v1+json"
        })
    void handlesBasicTypes(String mediaType) {
      assertThatCode(() -> MediaType.valueOf(mediaType)).doesNotThrowAnyException();
    }

    @Test
    void handlesWildcard() {
      assertThatCode(() -> MediaType.valueOf("*/*")).doesNotThrowAnyException();
    }

    @Test
    void handlesWildcardSubtype() {
      MediaType actual = MediaType.valueOf("application/*");

      assertThat(actual.isWildcardType()).isFalse();
      assertThat(actual.isWildcardSubtype()).isTrue();
    }

    @Test
    void handlesWildcardSubtypeWithExtension() {
      MediaType actual = MediaType.valueOf("application/*+json");

      assertThat(actual.isWildcardType()).isFalse();
      assertThat(actual.isWildcardSubtype()).isTrue();
    }

    @ParameterizedTest
    @ValueSource(strings = {"*/*", "application/*", "application/*+json"})
    void handlesWildcards(String mediaType) {
      assertThatCode(() -> MediaType.valueOf(mediaType)).doesNotThrowAnyException();
    }

    @ParameterizedTest
    @MethodSource("me.jvt.http.mediatype.MediaTypeTest#qualityValues")
    void handlesQuality(String mediaType, double quality) {
      MediaType actual = MediaType.valueOf(mediaType);

      assertThat(actual.getQualityValue()).isEqualTo(quality);
    }

    @ParameterizedTest
    @ValueSource(strings = {"application/json;v=1", "application/json ; v=1"})
    void handlesParameters(String mediaType) {
      MediaType actual = MediaType.valueOf(mediaType);

      assertThat(actual.getParameters()).containsEntry("v", "1");
    }

    @ParameterizedTest
    @ValueSource(
        strings = {
          "application/json;v=1;q=0",
          "application/json;q=0;v=1",
          "application/json; q=0;  v=1"
        })
    void handlesMultipleParameters(String mediaType) {
      MediaType actual = MediaType.valueOf(mediaType);

      assertThat(actual.getParameters()).containsEntry("q", "0");
      assertThat(actual.getParameters()).containsEntry("v", "1");
    }

    @Test
    void handlesMultipleLengthsOfParams() {
      MediaType actual = MediaType.valueOf("plain/text;level=foo;charset=abcdef;q=0.1");

      assertThat(actual.getParameters()).containsEntry("level", "foo");
      assertThat(actual.getParameters()).containsEntry("q", "0.1");
      assertThat(actual.getParameters()).containsEntry("charset", "abcdef");
    }
  }

  @Nested
  class ParseAcceptHeader {

    @Test
    void handlesSingle() {
      List<MediaType> mediaTypes = MediaType.parseAcceptHeader("application/json");

      assertThat(mediaTypes).containsExactlyInAnyOrder(MediaType.APPLICATION_JSON);
    }

    @ParameterizedTest
    @ValueSource(
        strings = {
          "application/json,text/plain",
          "application/json, text/plain",
          "application/json , text/plain"
        })
    void handlesMultiple(String acceptHeader) {
      List<MediaType> mediaTypes = MediaType.parseAcceptHeader(acceptHeader);

      assertThat(mediaTypes)
          .containsExactlyInAnyOrder(MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN);
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"ap"})
    void returnsEmptyListWhenCannotParse(String acceptHeader) {
      List<MediaType> mediaTypes = MediaType.parseAcceptHeader(acceptHeader);

      assertThat(mediaTypes).isEmpty();
    }
  }

  @Nested
  class ToString {
    @Test
    void wildcard() {
      assertThat(MediaType.WILDCARD).hasToString("*/*");
    }

    @Test
    void wildcardSubtype() {
      assertThat(MediaType.valueOf("application/*")).hasToString("application/*");
    }

    @Test
    void whenNoParameters() {
      assertThat(MediaType.APPLICATION_JSON).hasToString("application/json");
    }

    @Test
    void withQuality() {
      assertThat(MediaType.valueOf("application/foo;q=0")).hasToString("application/foo;q=0");
    }

    @Test
    void withOtherParamsPresentsAlphabeticallyAfterQuality() {
      assertThat(MediaType.valueOf("application/foo;level=1;abc=def;q=0"))
          .hasToString("application/foo;q=0;abc=def;level=1");
    }
  }

  @Nested
  class Comparator {
    @Test
    void isInstanceOfMediaTypeComparator() {
      assertThat(MediaType.COMPARATOR).isInstanceOf(MediaTypeComparator.class);
    }
  }

  @Nested
  class From {
    @Nested
    class SpringHttp {

      @Test
      void parsesWildcard() {
        MediaType parsed =
            MediaType.from(org.springframework.http.MediaType.valueOf("application/*"));

        assertThat(parsed.isWildcardSubtype()).isTrue();
      }

      @Test
      void parsesWithQuality() {
        MediaType parsed =
            MediaType.from(org.springframework.http.MediaType.valueOf("text/plain;q=0.5"));

        assertThat(parsed).hasToString("text/plain;q=0.5");
      }
    }
  }

  static Stream<Arguments> qualityValues() {
    return Stream.of(
        Arguments.of("application/json;q=0.1", 0.1),
        Arguments.of("application/json;q=0.10", 0.1),
        Arguments.of("application/json;q=0.100", 0.1),
        Arguments.of("application/json;q=0", 0),
        Arguments.of("application/json;q=1", 1),
        Arguments.of("application/json ; q=1", 1),
        Arguments.of("application/json;q=1.0", 1),
        Arguments.of("application/json;q=0.123", 0.123));
  }
}
