package me.jvt.http.mediatype;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;

class MediaTypeComparatorTest {

  private final MediaTypeComparator comparator = new MediaTypeComparator();

  private MediaType lhs;
  private MediaType rhs;

  @Test
  void wildcardTypeIsEqualToItself() {
    lhs = MediaType.WILDCARD;
    rhs = MediaType.WILDCARD;

    assertEqual();
  }

  @Test
  void wildcardTypeHasLowerPrecedenceWhenRhs() {
    lhs = new MediaType("audio", "*");
    rhs = MediaType.WILDCARD;

    assertLhs();
  }

  @Test
  void wildcardTypeHasLowerPrecedenceWhenLhs() {
    lhs = MediaType.WILDCARD;
    rhs = new MediaType("audio", "*");

    assertRhs();
  }

  @Test
  void nonWildcardAreEqual() {
    lhs = MediaType.APPLICATION_JSON;
    rhs = MediaType.MULTIPART_FORM_DATA;

    assertEqual();
  }

  @Test
  void wildcardSubtypeIsEqualToItself() {
    lhs = new MediaType("audio", "*");
    rhs = new MediaType("audio", "*");

    assertEqual();
  }

  @Test
  void wildcardSubtypeHasLowerPrecedenceWhenRhs() {
    lhs = new MediaType("audio", "basic");
    rhs = new MediaType("audio", "*");

    assertLhs();
  }

  @Test
  void wildcardSubtypeHasLowerPrecedenceWhenLhs() {
    lhs = new MediaType("audio", "*");
    rhs = new MediaType("audio", "basic");

    assertRhs();
  }

  @Test
  void qualityIsComparedBeforeSizeOfParameters() {
    Map<String, String> rhsParams = new HashMap<>();
    rhsParams.put("foo", "bar");
    rhsParams.put("q", "0.3");

    lhs = new MediaType("audio", "basic", Collections.singletonMap("q", "0.7"));
    rhs = new MediaType("audio", "basic", rhsParams);

    assertLhs();
  }

  @Test
  void qualityValueDefaultsTo1WhenNotFound() {
    lhs = new MediaType("audio", "*", Collections.singletonMap("q", "0.1"));
    rhs = new MediaType("audio", "basic");

    assertRhs();
  }

  @ParameterizedTest
  @NullAndEmptySource
  void qualityValueDefaultsTo1WhenNullOrEmtpy(String quality) {
    lhs = new MediaType("audio", "*", Collections.singletonMap("q", "0.1"));
    rhs = new MediaType("audio", "basic", Collections.singletonMap("q", quality));

    assertRhs();
  }

  @Test
  void sizeOfParametersDictatesPrecedenceWhenLhs() {
    lhs = new MediaType("audio", "basic", Collections.singletonMap("foo", "bar"));
    rhs = new MediaType("audio", "basic");

    assertLhs();
  }

  @Test
  void sizeOfParametersDictatesPrecedenceWhenRhs() {
    lhs = new MediaType("audio", "basic");
    rhs = new MediaType("audio", "basic", Collections.singletonMap("foo", "bar"));

    assertRhs();
  }

  @Test
  void sizeOfParametersWhenEqual() {
    lhs = new MediaType("audio", "basic", Collections.singletonMap("foo", "bar"));
    rhs = new MediaType("audio", "basic", Collections.singletonMap("foo", "bar"));

    assertEqual();
  }

  /*
  Via https://tools.ietf.org/html/rfc7231#section-5.3.2
   */
  @Nested
  class RfcExamples {

    @Test
    void audio() {
      List<MediaType> sorted = new ArrayList<>();
      MediaType basic = MediaType.valueOf("audio/basic");
      MediaType quality = MediaType.valueOf("audio/*; q=0.2");

      // purposefully reversed order
      sorted.add(quality);
      sorted.add(basic);

      sorted.sort(comparator);

      assertThat(sorted).containsExactly(basic, quality);
    }

    @Test
    void moreElaborateExample() {
      List<MediaType> sorted = new ArrayList<>();
      MediaType plain = MediaType.valueOf("text/plain; q=0.5");
      MediaType html = MediaType.valueOf("text/html");
      MediaType xdvi = MediaType.valueOf("text/x-dvi; q=0.8");
      MediaType xc = MediaType.valueOf("text/x-c");

      // purposefully reversed order
      sorted.add(plain);
      sorted.add(xdvi);
      sorted.add(xc);
      sorted.add(html);

      sorted.sort(comparator);

      List<MediaType> primary = new ArrayList<>();
      primary.add(sorted.get(0));
      primary.add(sorted.get(1));

      assertThat(primary).containsExactlyInAnyOrder(html, xc);

      assertThat(sorted.get(2)).isEqualTo(xdvi);
      assertThat(sorted.get(3)).isEqualTo(plain);
    }

    @Test
    void mediaRanges() {
      List<MediaType> sorted = new ArrayList<>();
      MediaType flowed = MediaType.valueOf("text/plain;format=flowed");
      MediaType plain = MediaType.valueOf("text/plain");
      MediaType text = MediaType.valueOf("text/*");

      // purposefully reversed order
      sorted.add(MediaType.WILDCARD);
      sorted.add(text);
      sorted.add(plain);
      sorted.add(flowed);

      sorted.sort(comparator);

      assertThat(sorted).containsExactly(flowed, plain, text, MediaType.WILDCARD);
    }

    @Test
    void anotherExample() {
      List<MediaType> sorted = new ArrayList<>();
      MediaType text = MediaType.valueOf("text/*;q=0.3");
      MediaType htmlQuality = MediaType.valueOf("text/html;q=0.7");
      MediaType htmlLevel = MediaType.valueOf("text/html;level=1");
      MediaType htmlLevelAndQuality = MediaType.valueOf("text/html;level=2;q=0.4");

      // purposefully reversed order
      sorted.add(htmlLevelAndQuality);
      sorted.add(htmlLevel);
      sorted.add(htmlQuality);
      sorted.add(text);

      sorted.sort(comparator);

      assertThat(sorted).containsExactly(htmlLevel, htmlQuality, htmlLevelAndQuality, text);
    }
  }

  void assertEqual() {
    assertThat(comparator.compare(lhs, rhs)).isZero();
  }

  void assertLhs() {
    assertThat(comparator.compare(lhs, rhs)).isEqualTo(-1);
  }

  void assertRhs() {
    assertThat(comparator.compare(lhs, rhs)).isEqualTo(1);
  }
}
